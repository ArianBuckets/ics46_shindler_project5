#include "proj5.hpp"
#include "MyPriorityQueue.hpp"
#include <queue>
#include <iostream>
#include <algorithm>
#include <iostream>

// As a reminder, for this project, edges have positive cost, g[i][j] = 0 means no edge.
std::vector<Edge> compute_mst(std::vector< std::vector<unsigned>> & graph)
{
	std::vector<Edge> mst;



	std::vector<bool> inTree(graph.size(),false); 
	std::vector<unsigned> dist (graph.size(), 9999);
	std::vector<int> parent (graph.size(),-1);
	dist[0] = 0;

	// start at vertex 0. for u: Go thru all of its adj matrix elements 
	//		call them v, if inTree[j] == false and 
	// 		dist[v] best_weight_yet > graph[u][v] current_weight update dist[v] = graph[u][v]
	//															and update parent[v] = u

	MyPriorityQueue<Edge> best_edge;
	best_edge.insert(Edge(0,0,0));
	

	// for (unsigned i = 0; i < graph.size(); i++)
	while (best_edge.isEmpty() == false)
	{
		Edge min_edge = best_edge.min();	
		best_edge.extractMin();
		inTree[min_edge.pt2] = true;
		// go thru adj mtx
		for (unsigned v=0; v < graph.size(); v++)
		{
			if (inTree[v] == false && dist[v] > graph[min_edge.pt2][v])
			{
				unsigned weight = graph[min_edge.pt2][v];
				parent[v] = min_edge.pt2;
				Edge to_add = Edge(min_edge.pt2,v,weight);
				best_edge.insert(to_add);
				dist[v] = graph[min_edge.pt2][v];
			}
		}
		
	}
	

	for (unsigned i = 1; i < graph.size(); i++)
	{
		mst.push_back(Edge(parent[i],i,dist[i]));
	}
	


	return mst;
}


// Returns the cost of the edges in the given vector,
// This does not confirm that it is a MST at all.  
unsigned mstCost(const std::vector<Edge> & vE) 
{
	unsigned sum =0;
	for(auto e: vE)
	{
		sum += e.weight;
	}
	return sum;
}




/* Old useless versions



	


	
	// // std::vector<bool> inTree(graph.size(),false); 
	// // std::vector<unsigned> dist (graph.size(), 9999);
	// // distances[0] = 0;

	// for (unsigned int i = 0; i < graph.size(); i++)
	// {
	// 	// inTree[i] = true;
	// 	MyPriorityQueue<Edge> min_nums;
	// 	for (unsigned int j = 0; j < graph[i].size(); j++)
	// 	{
	// 		// std:: cout << graph[i][j] << " ";
	// 		if (graph[i][j] == 0)
	// 		{
	// 			continue;
	// 		}
	// 		unsigned weight = graph[i][j];
	// 		// if (i == 0)
	// 		// {
	// 		// 	min_nums.insert(Edge(i,0,0));
	// 		// }
	// 		// else
	// 		// {
	// 		// min_nums.insert(Edge(i,j,weight));
	// 		 min_nums.insert(Edge(i,j,weight));
	// 		// }
			

	// 	}
	// 	auto the_min = min_nums.min();
	// 	mst.push_back(the_min);
	// 	// std::cout << std::endl;
	// }
	




	// for (unsigned int i = 0; i<graph.size(); ++i)
	// {
	// 	MyPriorityQueue<unsigned> vertex_weight;
	// 	for (unsigned int j = 0; j < graph[i].size(); j++)
	// 	{
	// 		// go thru the vector ex {} { 0, 12, 12, 13, 17, 9, 7, 14, 3, 17}
	// 		if (graph[i][j] == 0)
	// 		{
	// 			continue;
	// 		}
			
 	// 		vertex_weight.insert(graph[i][j]);

			
	// 	}
		
	// 	unsigned int the_min = vertex_weight.min(); // start vertex, dest, weight
		
	// 	unsigned int dest = 0;
	// 	// unsigned indx = 0
	// 	for (unsigned int k = 0; k < graph[i].size(); k++)
	// 	{	
	// 		if (graph[i][k] == the_min)
	// 		{
	// 			dest = k;
	// 			break;
	// 		}
	// 	}
		
	// 	mst.push_back(Edge(i,dest,the_min));
		
	// 	// add to priority queue
	// }


	

	// for (unsigned int i = 0; i<graph.size(); ++i)
	// {
	// 	inTree[i] = true;
	// 	MyPriorityQueue<Edge> temp_hold;
	// 	std::vector<unsigned> the_weights(graph.size(), 999999);
	// 	the_weights[0] = 0;
	// 	for (unsigned int j = 0; j < graph[i].size(); j++)
	// 	{			
	// 		// if (graph[i][j] == 0)
	// 		// {
	// 		// 	// temp_hold.insert(Edge())
	// 		// 	continue;
	// 		// }
	// 		if (inTree[j] == false && the_weights[j] > graph[i][j])
	// 		{
	// 			auto the_edge = Edge(Edge(i,j,graph[i][j]));
 	// 			temp_hold.insert(the_edge);
	// 			the_weights[j] = graph[i][j];
	// 		}	
	// 	}
	// 	std::cout << i << std::endl;
	// 	auto the_min = temp_hold.min(); // start vertex, dest, weight
	// 	mst.push_back(the_min);
	// }

*/