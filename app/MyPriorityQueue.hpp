#ifndef __PROJ5_PRIORITY_QUEUE_HPP
#define __PROJ5_PRIORITY_QUEUE_HPP

#include "runtimeexcept.hpp"
#include <vector>
#include <algorithm>

class PriorityQueueEmptyException : public RuntimeException 
{
public:
	PriorityQueueEmptyException(const std::string & err) : RuntimeException(err) {}
};




template<typename Object>
class MyPriorityQueue
{
private:
	// fill in private member data here
	std::vector<Object> vector_queue;


public:

	// You also need a constructor and a destructor.
	
	// constructor
	MyPriorityQueue();

	// destructor
	~MyPriorityQueue();

 	size_t size() const noexcept;
	bool isEmpty() const noexcept;

	void insert(const Object & elem);


	// Note:  no non-const version of this one.  This is on purpose because
	// the interior contents should not be able to be modified due to the
	// heap property.  This isn't true of all priority queues but 
	// for this project, we will do this.
	// min and extractMin should throw PriorityQueueEmptyException if the queue is empty.
	const Object & min() const; 

	void extractMin(); 

	void RecHeapify(std::vector<Object> & the_vec, unsigned int current_index); // Source: Wikipedia

};


template<typename Object>
MyPriorityQueue<Object>::MyPriorityQueue()
{
	
}


template<typename Object>
MyPriorityQueue<Object>::~MyPriorityQueue()
{

}



template<typename Object>
size_t MyPriorityQueue<Object>::size() const noexcept
{
	return vector_queue.size();
}



template<typename Object>
bool MyPriorityQueue<Object>::isEmpty() const noexcept
{
	if (this->vector_queue.size() == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

template<typename Object>
void MyPriorityQueue<Object>::insert(const Object & elem) 
{
	if (this->size() == 0)
	{
		// if empty queue, just push back.
		vector_queue.push_back(elem);
	}
	else
	{
		vector_queue.push_back(elem);
		unsigned int current_index = this->size() -1;
		Object parent = vector_queue.at(((current_index-1)/2));
		unsigned int parent_index = ((current_index-1)/2);
		while (current_index > 0)
		{

			// swap elem with parent
			parent = vector_queue.at(((current_index-1)/2));
			parent_index = ((current_index-1)/2);

			//---------/// NOTE*** don't forget, u changed >= to > -------------------------
			// if (vector_queue.at(current_index) >= vector_queue.at(parent_index))
			if ((vector_queue.at(parent_index) < vector_queue.at(current_index)) || 
				(vector_queue.at(parent_index) == vector_queue.at(current_index)))
			{
				break;
			}

			std::swap(vector_queue.at(current_index),vector_queue.at(parent_index));
			current_index = parent_index;
			

		}
	}
	RecHeapify(vector_queue,0);
	 
}




template<typename Object>
const Object & MyPriorityQueue<Object>::min() const
{
	
	return this->vector_queue.at(0);
}


template<typename Object>
void MyPriorityQueue<Object>::RecHeapify(std::vector<Object> & the_vec, unsigned int current_index)
{
	unsigned int left = 2 * current_index + 1;
	unsigned int right= 2 * current_index + 2;
	unsigned int smallest = current_index;

	if (left < the_vec.size())
	{
		if(the_vec.at(left) < the_vec.at(smallest))
		{
			smallest = left;
		}
		
	}
	if ( right < the_vec.size()  )
	{
		if(the_vec.at(right) < the_vec.at(smallest))
		{
			smallest = right;
		}
		
	}

	if (!(smallest == current_index))
	{
		std::swap(the_vec[current_index],the_vec[smallest]);
		this->RecHeapify(the_vec, smallest);
	}

}


template<typename Object>
void MyPriorityQueue<Object>::extractMin() 
{
	if (isEmpty())
	{
		throw PriorityQueueEmptyException("Unable To Extract From Empty Queue");
	}
	// swap max with min. Meaning swap end with begin. 
	// then delete last element, then heapify
	std::swap(vector_queue[0],vector_queue.back());
	vector_queue.pop_back();
	RecHeapify(vector_queue,0);
	
}


#endif 
